=== Social Media Feed Importer ===

Contributors: imagedesign

Tags: social media, facebook, twitter, instagram, flickr, pinterest, youtube, stream

Requires at least: 5.0

Tested up to: 5.2.4

Stable tag: 3.1.5

License: GPLv2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html

Show your most recent social media posts on your website. Support for Facebook, Twitter, Instagram, YouTube and Pinterest.

== Description ==

Show your most recent social media posts on your website. Support for Facebook, Twitter, Instagram, YouTube and Pinterest. Just add the shortcode [socialstream limit=3]

The shortcode includes 1 attribute: **limit** - set the number of social posts that should show within your stream.

== Installation ==

1. Unzip social-stream.zip
1. Upload the `social-stream` directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Find the Social Stream menu to add your settings

== Upgrade Notice ==

= 3.1.5 =

- no tags excerpt fix (wasn't shortening content)

= 3.1.4 =

- prevent duplicate content when the next post has no content, add option for plain text excerpt

= 3.1.3 =

- renable Facebook feeds

= 3.1.2 =

- disabled Facebook temporarily while awaiting approval process. Added multi Instagram account with tags.

= 3.1.0 =

- removed htmlentities and iconv to fix issue with importing emojis and special characters, set Facebook tags as children

= 3.0.8 =

- add htmlentities to Instagram feed content to fix special char issues preventing database insertion.

= 3.0.7 =

- add Standalone functionality based on \$mode variable in plugin. Include on any php page.

= 3.0.6 =

- fix img variable carry over into empty photo posts _May 3, 2019_

= 3.0.5 =

- Exclude Social post type from search, various Notice fixes. _May 3, 2019_

= 3.0.4 =

- Add Instagram Hashtag option. _April 30, 2019_

= 3.0.3 =

- Add Instagram Json import from hidden script on user's profile page. _April 23, 2019_

= 3.0.2 =

- Add placholder to [img] tag and add Git updater support

= 3.0.0 =

- Removed Flickr options - company no longer exists
- Added tag labels for multiple Facebook accounts so you can have separate feeds for each on front end - tags are just new service taxonomy terms, so use the single var to set per function call
- Added preg_replace for FetchRSS garbage code.
- Added htmlentities fix for special characters in code
- Removed Similar check - everything is based on date/tag for duplication checking.

= 2.9.8 =

- Changed max number of posts, fixed RSS feed without quotes on src

= 2.9.7 =

- Changed return function to return instead of echo

= 2.9.6 =

- Shortcode attributes variable mismatch fixed

= 2.9.5 =

- Escape duplicate search string to prevent error

= 2.9.4 =

- Fix for PHP7 compatibility

= 2.9.1 =

- Fix for similar posts starting with images - strip tags so only text is tested

= 2.9 =

- Add placeholder image option for posts without an image

= 2.5 =

- Support for multiple feeds of same service
- Facebook message import fixed
- YouTube Channel import updated

= 2.2 =

- Fixed [img] so it shows URL
- Added Wrap Tag Class field to settings

= 1.8 =

- Added Facebook API support due to disabled RSS feeds

= 1.4 =

- Fixed YouTube import issues

= 1.3 =

- Added Twitter API import due to disabled RSS feeds

= 1.2 =

- Added option for [photo|icon] in template

= 1.1 =

- Added rewrite rule to register_post_type rule
- Added import check to verify feed exists before importing

= 1.0 =

- First release
