<?php
/*
Plugin Name: Social Media Feed Importer
Description: Import your most recent social media posts on your website.  Support for Facebook, Twitter, Instagram, YouTube, and Pinterest.
Author: Marcus Vanstone
Author URI: http://imagedesign.pro
Version: 3.1.5
Date: July 28, 2020
GitLab Plugin URI: https://gitlab.com/idp-wp-plugins/idp-social-media-feed

STANDALONE INSTRUCTIONS:
1. Change $mode below to 'standalone'
2. add the social media ID's to the $sa_options below for importing
3. create a folder called 'socialimg' in the same directory as this script location
4. Run the following MYSQL in phpMyAdmin under your database in the SQL tab:

CREATE TABLE `socialfeed` (
  `id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `excerpt` text NOT NULL,
  `date` datetime NOT NULL,
  `date_gmt` datetime NOT NULL,
  `photo` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

*/

$mode = 'wordpress'; //standalone
if( $mode == 'standalone'){
	$sa_options = array(
		//DATABASE SETTINGS:
		'database_host'					=> 'localhost',
		'database_name'					=> '',
		'database_user'					=> '',
		'database_password'				=> '',
		//SOCIAL MEDIA ID's:
		'facebook_id' 					=> false, //GET ID FROM https://findfb.id/
		'instagram_id' 					=> false, //Username
		'instagram_hashtag' 			=> false, //Hashtag no hashmarks
		'twitter_id' 					=> false, //Username
		'twitter_search' 				=> false, 
		'youtube_id'					=> false, //Channel ID
		'pinterest_id'					=> false, //Username
		'pinterest_board'				=> false,
		//ADDITIONAL OPTIONS
		'social_retain'					=> '30', //Days
		'social_date_format'			=> 'F j, Y',
		'social_date_option'			=> 'timeago', //timeago or date
		'social_date_adjust'			=> false, // "-7 hours" or "+ 3 hours" adjust for time zone issues
		'social_template'				=> "<p><span class='excerpt'>[excerpt]</span></p><p><span class='thumb'>[photo]</span><br><a class='date' href='[sourcelink]' target='_blank' rel='nofollow' title='View this post on [service]'>View on [service]</a></p>",
		'social_template_wrap'			=> 'section',
		'social_template_wrap_class'	=> 'social',
		'social_template_excerpt'		=> '30', //word length on [excerpt] 
		'social_lazyload'				=> false,  //true/false
		'social_photo_size'				=> false, //length, width in px
		'social_plimg'					=> false, //Placeholder Image URL
	) ;
}
if( !function_exists('get_option')){
	function get_option($option){
		global $sa_options;
		return $sa_options[$option];
	}			
}
if( get_option('twitter_id') || get_option('twitter_search')){
    require_once('tmhOAuth.php'); //Twitter
    require_once('tmhUtilities.php'); //Twitter
}
if ( $mode == 'wordpress' && !function_exists('media_handle_upload') ) {
	require_once(ABSPATH . 'wp-admin/includes/media.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');
}

class SocialParser{
	var $mode;
	function __construct(){
        global $mode;
        $this->mode = $mode;
		if( $this->mode == 'wordpress'){
			//echo 'WHY NO?';
			add_action('init', array($this, 'add_custom_types') );
			add_action('admin_menu', array($this, 'admin_menu'));
			add_action('admin_init', array($this, 'register_settings') );
			add_action('admin_menu', array($this,'RemoveAddNewMenu'));
			add_action('admin_head', array($this,'RemoveAddNewButton'));
			add_action('social_schedule', array($this, 'schedule') );
			add_action('wp', array($this, 'activate_schedule') );
			if( isset($_POST['social-update-now']) ){
				add_action("admin_init", array($this, 'CreateTerms'));
				add_action('admin_init', array($this, 'schedule') );
			} 
			//register_activation_hook( __FILE__, array($this, 'add_terms') );
			add_filter('manage_edit-social_post_columns', array( $this, 'addSocialCols'));
			add_action( 'manage_social_post_posts_custom_column', array( $this, 'manageSocialCols'), 10, 2);
			add_shortcode('socialfeed', array($this, 'Shortcode') );
			add_shortcode('socialstream', array($this, 'Shortcode') );
			if( isset($_POST['social-kill-all']) ) add_action('init', array($this, 'delete_posts') );
			add_theme_support( 'post-thumbnails', array( 'social_post' ) );
			add_action("admin_enqueue_scripts", array($this, "enqueue_media_uploader"));
			if( isset($_GET['page']) && isset($_GET['settings-updated']) && $_GET['page'] == 'social-settings' && $_GET['settings-updated'] == true){
				add_action("admin_init", array($this, 'CreateTerms'));

			}
		}
		if( $this->mode == 'standalone'){
			$db_host = get_option('database_host');
			$db_name = get_option('database_name');
			$db_user = get_option('database_user');
			$db_password = get_option('database_password');

			try
			{
				$this->db = new PDO("mysql:host=$db_host;port=3306;dbname=$db_name", $db_user, $db_password);
			}
			catch(PDOException $e) 
			{
				die ('Database connection failed: '. $e->getMessage());
			}
			
			if( isset($_GET['update-now']) ) $this->schedule(); 
			
			if( isset($_GET['kill-all']) ) $this->delete_posts();
		}
	}
	function enqueue_media_uploader(){
    	wp_enqueue_media();
	}

	function activate_schedule(){
		if( !wp_next_scheduled( 'social_schedule' ) ){
			wp_schedule_event( current_time( 'timestamp' ) , 'hourly', 'social_schedule');
		}
	}
	function CreateTerms(){
		$fbtags = get_option('facebook_tag');
		if( count($fbtags) > 0){
			$fbterm = term_exists( 'Facebook', 'social');
			if( !$fbterm )
				$fbterm = wp_insert_term( 'Facebook', 'social');
			//echo '<pre>'.print_r($fbterm,1).'</pre>'; //exit;
			foreach( $fbtags as $kid){
				if( !term_exists( $kid, 'social', $fbterm['term_id']))
					wp_insert_term( $kid, 'social', array( 'parent' => $fbterm['term_id']));
			}
		}
		$igtags = get_option('instagram_tag');
		if( count($igtags) > 0){
			$igterm = term_exists( 'Instagram', 'social');
			if( !$igterm )
				$igterm = wp_insert_term( 'Instagram', 'social');
			//echo '<pre>'.print_r($igterm,1).'</pre>'; exit;
			foreach( $igtags as $kid){
				if( !term_exists( $kid, 'social', $igterm['term_id']))
					wp_insert_term( $kid, 'social', array( 'parent' => $igterm['term_id']));
			}
		}
	}
	function add_terms(){
		if( !term_exists( 'Facebook', 'social' ) ) wp_insert_term('Facebook', 'social');
		if( !term_exists( 'Twitter', 'social' ) ) wp_insert_term('Twitter', 'social');
		if( !term_exists( 'Twitter Search', 'social' ) ) wp_insert_term('Twitter Search', 'social');
		if( !term_exists( 'YouTube', 'social' ) ) wp_insert_term('YouTube', 'social');
		if( !term_exists( 'YouTube Favorite', 'social' ) ) wp_insert_term('YouTube Favorite', 'social');
		if( !term_exists( 'Instagram', 'social' ) ) wp_insert_term('Instagram', 'social');
		if( !term_exists( 'Instagram Hashtag', 'social' ) ) wp_insert_term('Instagram Hashtag', 'social');
		if( !term_exists( 'Pinterest', 'social' ) ) wp_insert_term('Pinterest', 'social');
	}
	function admin_menu(){
		add_submenu_page( 'edit.php?post_type=social_post', 'Settings', 'Settings', 'activate_plugins', 'social-settings', array($this, 'settings_panel'));
	}
	function register_settings(){
		register_setting( 'social-rss-feeds', 'social_retain');
		register_setting( 'social-rss-feeds', 'social_date_format');
		register_setting( 'social-rss-feeds', 'social_date_option');
		register_setting( 'social-rss-feeds', 'social_date_adjust');
		register_setting( 'social-rss-feeds', 'facebook_tag');
    	register_setting( 'social-rss-feeds', 'facebook_id');
		register_setting( 'social-rss-feeds', 'twitter_id');
		register_setting( 'social-rss-feeds', 'twitter_search');
		register_setting( 'social-rss-feeds', 'youtube_id');
		register_setting( 'social-rss-feeds', 'youtube_favs');
		register_setting( 'social-rss-feeds', 'instagram_tag');
		register_setting( 'social-rss-feeds', 'instagram_id');
		register_setting( 'social-rss-feeds', 'instagram_hashtag');
		register_setting( 'social-rss-feeds', 'pinterest_id');
		register_setting( 'social-rss-feeds', 'pinterest_board');
		register_setting( 'social-rss-feeds', 'social_template');
		register_setting( 'social-rss-feeds', 'social_template_wrap');
		register_setting( 'social-rss-feeds', 'social_template_wrap_class');
		register_setting( 'social-rss-feeds', 'social_template_excerpt');
		register_setting( 'social-rss-feeds', 'social_photo_size');
		register_setting( 'social-rss-feeds', 'social_lazyload');
		register_setting( 'social-rss-feeds', 'social_plimg');
	}

	function get_feed($id, $type, $board=false){
		switch($type):
			case 'facebook':
				//return 'https://www.facebook.com/feeds/page.php?id='.$id.'&format=rss20';
				return 'https://graph.facebook.com/v3.3/'.$id.'/posts?access_token=411940298995603|qQMYUzR9rSCaozwdahEVVxNv4BE&fields=attachments,created_time,message';
				//return urlencode($id);
				break;
			case 'twitter':
				return array('timeline', urlencode($id) );
				break;
			case 'twittersearch':
				return array('search', urlencode($id) );
				break;
			case 'youtube':
				return 'https://www.youtube.com/feeds/videos.xml?channel_id='.$id;
				break;
			case 'instagram':
				return 'https://www.instagram.com/'.$id;
				break;
			case 'instagram_hashtag':
				return 'https://www.instagram.com/explore/tags/'.$id;
				break;
			case 'pinterest':
				if( $board ) return 'http://pinterest.com/'.$id.'/'.$board.'.rss';
				return 'http://pinterest.com/'.$id.'/feed.rss';
				break;
		endswitch;
	}

	function delete_posts($before=false){
		//global $mode;
		if( $this->mode == 'wordpress' ){
			global $wpdb;
			if( $before ) $before = "AND post_date < '".date('Y-m-d', strtotime($before.' days ago'))." 00:00:00'";
			$social = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_type='social_post' $before");
			foreach($social as $s){
				$id = $s->ID;
				$attachments = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_parent=$id");
				foreach( $attachments as $attID ){
					wp_delete_attachment( $attID->ID, 1 );
				}
				wp_delete_post( $s->ID, true );
			}
		}elseif( $this->mode == 'standalone'){
            if( $before ) $before = "AND post_date < '".date('Y-m-d', strtotime($before.' days ago'))." 00:00:00'";
            //$stmt = $this->db->prepare("SELECT * FROM socialfeed WHERE id > 0 $before");
            $stmt = $this->db->prepare('SELECT * FROM socialfeed WHERE id > 0'.$before);
			$stmt->execute();
           
            $social = $stmt->fetchAll();
           // echo '<pre>'.print_r($social,1).'</pre>'; 
			
			foreach($social as $s){
                $id = $s['id'];
                $photo = getcwd().$s['photo'];
                unlink($photo); //delete photo
                //echo 'DELETE '.getcwd().$photo;
               // echo 'DELETE FROM socialfeed WHERE id = '.$id;
                $stmt2 = $this->db->prepare('DELETE FROM socialfeed WHERE id = :pid');
                $stmt2->bindParam(':pid', $id);	
                $stmt2->execute();
            }
            echo 'ALL SOCIAL POSTS DELETED!';
            exit;
		}
	}

	function settings_panel(){
		global $_wp_additional_image_sizes;
		$df = get_option("social_date_format");
		if( !$df ) $df = get_option('date_format');
		$do = get_option("social_date_option");
		$ta = get_option("social_date_adjust");
		$template = get_option("social_template");
		if( !$template ) $template = "<p>
<a class='date' href='[sourcelink]' target='_blank' rel='nofollow' title='View this post on [service]'>[icon] [postdate]</a><br>
<span class='thumb'>[photo]</span>
<span class='title'>[excerpt]</span>
</p>";
		$wrap = get_option("social_template_wrap");
		$wrapclass = get_option("social_template_wrap_class");
		if( !$wrapclass ) $wrapclass = 'social';
		if( !$wrap ) $wrap = 'section';
		?>
        <div class="wrap">
		<h2>Social Media Feed Settings</h2>
        <p>Use the shortcode <code>[socialfeed limit=3]</code> or the function <code>mySocialFeed($number_of_posts, $singlefeed, $templateoverride);</code> to display your stream on your website.</p>
        <form action="options.php" method="post">
        	<?php settings_fields( 'social-rss-feeds' );// do_settings( 'social-rss-feeds' );?>

			<table class="form-table">
               <!-- <thead>
               		 <tr><th colspan="2"><h3>Overall Settings</h3></th></tr>
                </thead> -->
                <tbody>
                	<tr><th colspan="2"><h3>Retain Settings</h3></th></tr>
                	<tr valign="top">
                		<th scope="row"><label for="social_retain">Retain Social Posts for:</label></th>
                		<td><label><input type="number" name="social_retain" id="social_retain"  value="<?php echo get_option("social_retain");?>"> Days</label>
                       </td>
                	</tr>
                    <tr><th colspan="2"><h3>Date Settings</h3></th></tr>
                	<tr valign="top">
                		<th scope="row"><label for="social_date_option">Date Option:</label></th>
                		<td><label><input type="radio" name="social_date_option" id="social_date_option"  value="date" <?php if( $do == 'date' ) echo 'checked="checked"';?> /> Show Date Posted</label> <label><input type="radio" name="social_date_option" id="social_date_option"  value="timeago" <?php if( $do == 'timeago' ) echo 'checked="checked"';?> /> Show Time Ago</label>
                       </td>
                	</tr>
                    <tr valign="top">
                		<th scope="row"><label for="social_date_format">Date Format:</label></th>
                		<td><input type="text" name="social_date_format" id="social_date_format" style="width:40%"  value="<?php echo $df; ?>" /><br><small><a href="http://codex.wordpress.org/Formatting_Date_and_Time" target="_blank">Documentation on date and time formatting</a></small>

                       </td>
                	</tr>

                    <tr valign="top">
                		<th scope="row"><label for="social_date_adjust">Time Adjustment:</label></th>
                		<td><input type="text" name="social_date_adjust" id="social_date_adjust" style="width:40%"  value="<?php echo $ta; ?>" /><br />
                         <small>e.g. "- 7 hours" or "+ 3 hours" to adjust for time zone issues</small>
                       </td>
                	</tr>

               		 <tr><th colspan="2"><h3>Feed Settings</h3></th></tr>
              
                	<tr valign="top">
                		<th scope="row"><label for="facebook_id">Facebook Page ID's #:</label></th>
                    <?php
    $fbtags = get_option('facebook_tag');
		$fbids = get_option("facebook_id");
		
    ?>
                		<td>
                      <input type="text" name="facebook_tag[]" id="facebook_tag" style="width:200px"  value="<?php echo $fbtags[0];?>" placeholder="Account Label">
                      <input type="text" name="facebook_id[]" id="facebook_id" style="width:200px"  value="<?php echo $fbids[0];?>" placeholder="Facebook ID"> 
                      <?php if( $fbids[0] ) echo '<a href="https://graph.facebook.com/v3.3/'.$fbids[0].'/posts?access_token=411940298995603|qQMYUzR9rSCaozwdahEVVxNv4BE&fields=attachments,created_time,message" target="_blank">Test Feed</a>';
    ?>
                      <br>
                      <input type="text" name="facebook_tag[]" style="width:200px"  value="<?php echo $fbtags[1];?>" placeholder="Account Label">
                      <input type="text" name="facebook_id[]" style="width:200px"  value="<?php echo $fbids[1];?>" placeholder="Facebook ID">
                      <?php if( $fbids[0] ) echo '<a href="https://graph.facebook.com/v3.3/'.$fbids[1].'/posts?access_token=411940298995603|qQMYUzR9rSCaozwdahEVVxNv4BE&fields=attachments,created_time,message" target="_blank">Test Feed</a>';
    ?>
                      
                      <br><small>If your facebook page does not end in your ID # (as it has a custom name url), you will need to use <a href="https://findfb.id/" target="_blank">FB ID Finder</a> (paste in page URL)</small>
                        <?php /*if( get_option("facebook_id") ){
							$ids = explode(',', str_replace( ' ', '', get_option("facebook_id") ));
							foreach( $ids as $id ) echo '<br><a href="https://graph.facebook.com/v2.6/'.$id.'/posts?access_token=411940298995603|qQMYUzR9rSCaozwdahEVVxNv4BE&fields=attachments,created_time,message" target="_blank">Test Feed</a>';
							}*/?>
                        </td>
                	</tr>
					<?php
    $igtags = get_option('instagram_tag');
		$igids = get_option("instagram_id");
		
    ?>

					<tr valign="top">
                		<th scope="row"><label for="instagram_id">Instagram Accounts:</label></th>
                		<td>
						<input type="text" name="instagram_tag[]" id="instagram_tag" style="width:200px"  value="<?php echo $igtags[0];?>" placeholder="Account Label">
                      <input type="text" name="instagram_id[]" id="instagram_id" style="width:200px"  value="<?php echo $igids[0];?>" placeholder="Instagram Account Name"> 
                        <?php if( $igids[0] ){ echo '<br><a href="'.$this->get_feed($igids[0], 'instagram').'/?__a=1" target="_blank">Test your Feed</a>';}?>
						<br>
						<input type="text" name="instagram_tag[]" style="width:200px"  value="<?php echo $igtags[1];?>" placeholder="Account Label">
                      <input type="text" name="instagram_id[]" style="width:200px"  value="<?php echo $igids[1];?>" placeholder="Instagram Account Name"> 
                        <?php if( $igids[1] ){ echo '<br><a href="'.$this->get_feed($igids[1], 'instagram').'/?__a=1" target="_blank">Test your Feed</a>';}?></td>
                	</tr>
									<tr valign="top">
                		<th scope="row"><label for="instagram_hashtag">Instagram Hashtag:</label></th>
                		<td><input type="text" name="instagram_hashtag" id="instagram_hashtag" style="width:40%"  value="<?php echo get_option("instagram_hashtag");?>" />
                        <?php if( get_option("instagram_hashtag") ){ echo '<br><a href="'.$this->get_feed(get_option('instagram_hashtag'), 'instagram_hashtag').'?__a=1" target="_blank">Test your Feed</a>';}?></td>
                	</tr>

                    <tr valign="top">
                		<th scope="row"><label for="twitter_id">Twitter username:</label></th>
                		<td><input type="text" name="twitter_id" id="twitter_id" style="width:40%"  value="<?php echo get_option("twitter_id");?>" />
                        <?php //if( get_option("twitter_id") ){ echo '<br><a href="'.$this->get_feed(get_option('twitter_id'), 'twitter').'" target="_blank">Test your Feed</a>';}?></td>
                	</tr>

                    <tr valign="top">
                		<th scope="row"><label for="twitter_search">Twitter search:</label></th>
                		<td><input type="text" name="twitter_search" id="twitter_search" style="width:40%"  value="<?php echo get_option("twitter_search");?>" />
                        <?php // if( get_option("twitter_search") ){ echo '<br><a href="'.$this->get_feed(get_option('twitter_search'), 'twittersearch').'" target="_blank">Test your Feed</a>';}?></td>
                	</tr>

                  <tr valign="top">
                		<th scope="row"><label for="youtube_id">YouTube Channel ID:</label></th>
                		<td><input type="text" name="youtube_id" id="youtube_id" style="width:40%"  value="<?php echo get_option("youtube_id");?>" />
                        <!-- <label> <select name="youtube_favs">
                        <option value="no" <?php if( get_option("youtube_favs") == 'no' ) echo 'selected="selected"';?> /> Show Uploads Only</option>
                        <option value="addfavs" <?php if( get_option("youtube_favs") == 'addfavs' ) echo 'selected="selected"';?> /> Show Uploads &amp; Favorites</option>
                        <option value="onlyfavs" <?php if( get_option("youtube_favs") == 'onlyfavs' ) echo 'selected="selected"';?> /> Show Favorites Only</option>

                        </select> </label> --><?php if( get_option("youtube_id") ){ echo '<br><a href="'.$this->get_feed(get_option('youtube_id'), 'youtube').'" target="_blank">Test your Feed</a>';}?>
                        <?php //if( get_option("youtube_favs") != 'no' ){ echo ' <a href="'.$this->get_feed(get_option('youtube_id'), 'youtubefavs').'" target="_blank">Test your Favorites Feed</a>';}?>
                        </td>
                	</tr>

                    


                    <tr valign="top">
                		<th scope="row"><label for="pinterest_id">Pinterest username:</label></th>
                		<td><input type="text" name="pinterest_id" id="pinterest_id" style="width:40%"  value="<?php echo get_option("pinterest_id");?>" /> <label>Specific Board Name: <input type="text" name="pinterest_board" id="pinterest_board" style="width:40%"  value="<?php echo get_option("pinterest_board");?>" /></label>
                        <?php if( get_option("pinterest_id") ){ echo '<br><a href="'.$this->get_feed(get_option('pinterest_id'), 'pinterest', get_option("pinterest_board")).'" target="_blank">Test your Feed</a>';}?>
                        </td>
                	</tr>


               		 <tr><th colspan="2"><h3>Output Settings</h3></th></tr>

                     <tr valign="top">
                		<th scope="row"><label for="social_template_wrap">Template Wrap Tag:</label></th>
                		<td><input type="text" name="social_template_wrap" id="social_template_wrap" style="width:20%" value="<?php echo $wrap; ?>"><br><small>The name of the html tag that will surround each post.  ie. div, li, section, article, etc.</small>
                       </td>
                	</tr>
                   <tr valign="top">
                		<th scope="row"><label for="social_template_wrap_class">Template Wrap Tag Class:</label></th>
                		<td><input type="text" name="social_template_wrap_class" id="social_template_wrap_class" style="width:20%" value="<?php echo $wrapclass; ?>"><br><small>Add the class(es) you'd like on your Wrap Tag. ie. col-sm-6, socialpost, etc.</small>
                       </td>
                	</tr>
                     <tr valign="top">
                		<th scope="row"><label for="social_template">Template:</label></th>
                		<td><textarea name="social_template" id="social_template" style="width:80%;height: 160px;"><?php echo $template;?></textarea><br><small>Tags:<br />
                        [sourcelink] = URL to original social media post<br />
                        [service] = social media service name<br />
                        [icon] = social media icon as saved in your {templatedir}/img/{servicename}.png<br />
                        [actions] = social media actions related to the service such as Like, Share, Retweet, etc.<br>
                        [postdate] = date or timeago of post<br />
                        [photo] = displays linked image if post included one<br />
                        [photo|icon] = displays linked image if included, otherwise displays icon instead<br />
                        [img] = URL of image in post<br />
                        [excerpt] = shortened post content<br />
						[excerpt_notags] = shortened post content without any HTML tags<br />
                        [content] = full post content<br />
                        </small>
                       </td>
                	</tr>

                    <tr valign="top">
                		<th scope="row"><label for="social_template_excerpt">Social Post Exerpt Word Length:</label></th>
                		<td><input type="text" name="social_template_excerpt" id="social_template_excerpt" style="width:40%" value="<?php echo get_option('social_template_excerpt'); ?>">
                       </td>
                	</tr>
                    <tr valign="top">
                		<th scope="row"><label for="social_template_excerpt">Lazy Load Images:</label></th>
                		<td><label><input type="checkbox" name="social_lazyload" id="social_lazyload" value="1" <?php if( get_option('social_lazyload')) echo 'checked="checked"'; ?>> Enable</label>
                       </td>
                	</tr>

                    <tr valign="top">
                		<th scope="row"><label for="social_photo_size">Social Photo Size:</label></th>
                		<td><select name="social_photo_size" id="social_photo_size">
                        <option<?php if( get_option('social_photo_size') == 'thumbnail') echo ' selected="selected"'; ?>>thumbnail</option>
                        <option<?php if( get_option('social_photo_size') == 'medium') echo ' selected="selected"'; ?>>medium</option>
                        <option<?php if( get_option('social_photo_size') == 'large') echo ' selected="selected"'; ?>>large</option>
  <?php foreach ($_wp_additional_image_sizes as $size_name => $size_attrs): ?>
    <option <?php if( get_option('social_photo_size') == $size_name) echo 'selected="selected"'; ?>><?php echo $size_name; ?></option>
  <?php endforeach; ?>
</select>
                       </td>
                	</tr>

                    <tr valign="top">
                		<th scope="row"><label for="social_plimg">Placeholder Image:</label></th>
                		<td class="plimg">
                        <?php $plid = get_option('social_plimg');  ?>
                        <input type="hidden" name="social_plimg" id="social_plimg" value="<?php echo $plid;?>">
               <?php if( $plid ){
					$plimg = wp_get_attachment_image_src($plid, get_option('social_photo_size'));
				    echo '<img src="'.$plimg[0].'" id="plimg"><strong class="dismiss" style="cursor:pointer; vertical-align:top;">X</strong><br>';

			   }
					 ?>
                <button type="button" class="btn btn-primary getmedia">Select/Upload Image</button>

                       </td>
                	</tr>


                </tbody>
			</table>
            <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
		</form>
        <script>
    //New jQuery JS Wrapper
    jQuery(window).bind("load", function() {
    jQuery(function($) {

      function open_media_uploader_image()
      {
          media_uploader = wp.media({
              frame:    "post",
              state:    "insert",
              multiple: false
          });

          media_uploader.on("insert", function(){
              var json = media_uploader.state().get("selection").first().toJSON();

              var pl_url = json.url;
			  var pl_id = json.id;

          console.log(json);
          $("#social_plimg").val(pl_id);
          $("#social_plimg").after('<img src="'+pl_url+'" id="plimg" style="max-width:300px;height:auto;"><strong class="dismiss" style="cursor:pointer;vertical-align:top">X</strong><br>');
          });

          media_uploader.open();
      }
      $(".getmedia").click(function(){
        open_media_uploader_image();
      });
      $(".plimg").on('click', '.dismiss', function(){
        $('#plimg, .dismiss').remove();
        $('#social_plimg').val( '' );
      });

    });
    });
    </script>

        <h2>Next Feed Update:
        <?php

		$next_cron = wp_next_scheduled('social_schedule');
			if ( !empty( $next_cron ) ) :
				$datetime = get_option('date_format') . ' @ ' . get_option('time_format');
				$curtime = date_i18n($datetime);
					?>

					<span title="Current time is: <?php echo $curtime;?>"><?php echo gmdate($datetime, $next_cron + (get_option('gmt_offset') * 3600)); ?></span>

					<?php
				else:
				echo 'No schedule available';
				endif;

		?></h2>
        <form action="edit.php?post_type=social_post" method="post"><p class="submit"><input type="submit" class="button-secondary" name="social-update-now" value="Manual Feed Import" /></p></form>

        <h3>Clear all Social Posts</h3>
        <form action="" method="post"><p class="submit"><input type="submit" class="button-secondary" name="social-kill-all" value="Permanently Delete all Social Posts" /><br /><small>WARNING: This cannot be undone!</small></p></form>
        </div>
        <?php
	}


	function add_custom_types(){
		if( !post_type_exists('social_post') ):
			register_post_type('social_post', array(	'label' => 'Social Posts','description' => '','public' => true,'show_ui' => true,'show_in_menu' => true,'capability_type' => 'page','hierarchical' => false,'rewrite' => array('slug' => 'social'),'query_var' => true,'exclude_from_search' => true,'supports' => array('title','editor','excerpt','thumbail'),'taxonomies' => array('social',),'labels' => array (
  'name' => 'Social Posts',
  'singular_name' => 'Social Post',
  'menu_name' => 'Social Feed',
  'add_new' => '',
  'add_new_item' => '',
  'edit' => '',
  'edit_item' => '',
  'new_item' => '',
  'view' => '',
  'view_item' => '',
  'search_items' => 'Search Social Posts',
  'not_found' => 'No Social Posts Found',
  'not_found_in_trash' => 'No Social Posts Found in Trash',
  'parent' => 'Parent Social Post',
),) );
		endif;
		if( !taxonomy_exists('social') ):
			register_taxonomy('social',array (
  0 => 'social_post',
),array( 'hierarchical' => true, 'label' => 'Services','show_ui' => true,'query_var' => true,'rewrite' => array('slug' => 'social'),'singular_label' => 'Service') );
		endif;

		//Custom Fields Add-on
		$cpi = get_option('cpi_files');
		if( !is_array($cpi) ) $cpi = array();
		if( !in_array('balloons.png', $cpi)){
			$cpi['social_post'] = 'balloons.png';
			update_option('cpi_files', $cpi);
		}
		if(function_exists("register_field_group")){
			register_field_group(array (
				'id' => '503e28505f663',
				'title' => 'Social',
				'fields' =>
				array (
					0 =>
					array (
						'label' => 'Link',
						'name' => 'social_link',
						'type' => 'text',
						'instructions' => '',
						'required' => '0',
						'default_value' => '',
						'formatting' => 'html',
						'key' => 'field_503d358a02d1f',
						'order_no' => '0',
					),
					1 =>
					array (
						'label' => 'Feature Photo',
						'name' => '_thumbnail_id',
						'type' => 'image',
						'instructions' => '',
						'required' => '0',
						'default_value' => '',
						'formatting' => 'html',
						'key' => 'field_503d358a036d5',
						'order_no' => '1',
					),

				),
				'location' =>
				array (
					'rules' =>
					array (
						0 =>
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'social_post',
							'order_no' => '0',
						),
					),
					'allorany' => 'all',
				),
				'options' =>
				array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' =>
					array (
						0 => 'custom_fields',
						1 => 'discussion',
						2 => 'comments',
						3 => 'slug',
						4 => 'author',
						5 => 'format',
						6 => 'featured_image',
					),
				),
				'menu_order' => 0,
			));
		}
	}

	function RemoveAddNewMenu() {
	  global $submenu;
	  unset($submenu['edit.php?post_type=social_post'][10]); // Add New
	 // unset($submenu['edit.php?post_type=social_post'][15]); // Services Categories
	}

	// hide "add new" button on edit page
	function RemoveAddNewButton() {
	  global $pagenow;
	  if(is_admin()){
		if($pagenow == 'edit.php' && $_GET['post_type'] == 'social_post'){
			echo '<style>.add-new-h2{display: none;}</style>';
		}
	  }
	}


	function addSocialCols($cols){
		$new['cb'] = '<input type="checkbox" />';
		$new['title'] = 'Posted';
		$new['service'] = 'Service';
		$new['spreview'] = 'Content';
		$new['sphoto'] = 'Photo';

		return $new;
	}


	function manageSocialCols($column_name, $id){
		//echo 'ID:'.$id;
		switch( $column_name ):
			case 'service':
				$terms = wp_get_post_terms( $id, 'social' );
				//print_r($terms);
				$link =  get_post_meta($id, 'social_link', true);
				echo '<a href="'.$link.'" target="_blank" title="View this item in a new tab">'.$terms[0]->name.'</a>';
				break;

			case 'spreview':
				$item = get_post( $id );
				echo wp_trim_words($item->post_excerpt, 15);
				break;


			case 'sphoto':
				$photo = get_post_meta($id, '_thumbnail_id', true);

				if( $photo ){
					$img = wp_get_attachment_image_src($photo, 'thumbnail');
					 echo  '<img src="'.$img[0].'" alt="" style="width: 100px; height:auto;" />';
				}
				break;

			default:
			echo '?';
				break;

		endswitch;
	}

	function fetch_facebook_json($data, $type){
		$opts = array(
			'http'=>array(
			'method'=>"GET",
			'header'=>"Accept: application/vnd.travify.v1+json\r\n"
			)
		);

		$context = stream_context_create($opts);
		//$output = file_get_contents($data, false, $context);
		$string = file_get_contents($data, false, $context);
		$result = json_decode($string, true);

		$fb_array = array();
		//echo '<pre>'.print_r($result['data'],1).'</pre>'; exit;
		foreach( $result['data'] as $dt ){
			$desc = false;
			$id = explode('_', $dt['id']);
			$title = 'POST #'.$id[1];
			if( isset($dt['message']) ) $desc = $dt['message'];
			//$desc = htmlentities($dt['message']);
			if( !$desc && isset($dt['attachments']['data'][0]['description']) ) $desc = $dt['attachments']['data'][0]['description'];
			if( !$desc && isset($dt['attachments']['data'][0]['subattachments'][0]['description'])) $desc = $dt['attachments']['data'][0]['subattachments'][0]['description'];
			//if( !$desc ) $desc = $dt['attachments']['data'][0]['subattachments'][1]['description'];
			 //Converts UTF-8 into ISO-8859-1 to solve special symbols issues
			//$desc = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $desc);
			//echo $desc.'<br><br><hr><br>';
			//Get status update time
			$pubdate = strtotime($dt['created_time']);
			$propertime = gmdate('F jS Y, H:i', $pubdate);  //Customize this to your liking

			//Get link to update

			$linkback = 'https://www.facebook.com/'.$id[0].'/posts/'.$id[1];
			//$linkback = $dt['attachments']['data'][0]['url'];
			//$linkback = (string)$linkback[0];

			//Photo
			if( isset($dt['attachments']['data'][0]['media'])){
				$photo = $dt['attachments']['data'][0]['media']['image']['src'];
				if( !$photo ){ 
					$photo = $dt['attachments']['data'][0]['subattachments']['data'][0]['media']['image']['src'];
				}
				//print_r($dt['attachments']); echo $photo; exit;
			}

				   //Store values in array
				   $fb_item = array(
						  'desc' => $desc,
						  'title' => $desc,
						  'date' => $propertime,
						  'link' => $linkback,
						  'photo' => $photo
				   );
				   array_push($fb_array, $fb_item);
		}
		//echo '<pre>'.print_r($fb_array,1).'</pre>'; exit;
		return $fb_array;
	}

	function fetch_twitter_api($data, $type){

		$tmhOAuth = new tmhOAuth(array(
		  'consumer_key'    => 'OkY25f0dqRBo3aC0UoqWw',
		  'consumer_secret' => 'pCv7ZL8NpeZ1OH153l9J7jeJOTZKoo8u1GSt0soy2Yg',
		  'user_token'      => '20249776-3J59RwyVNnnpCAkx06G0qtzWDNEUwrVpn7fi5qKNL',
		  'user_secret'     => 'EomAeKVDUozLigTq1jQ8Wl5feaD6kIrlx8ZQyXUNUE',
		  'curl_ssl_verifypeer'   => false
		));

		if( $data[0] == 'timeline'):

			$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/user_timeline'), array('screen_name' => $data[1], 'include_entities' => true, 'include_rts' => true, 'exclude_replies' => true, count => 15 ));
			$response = $tmhOAuth->response;
			$prelink = 'https://twitter.com/'.$data[1].'/status/';

		else:
			//https://api.twitter.com/1.1/search/tweets.json?q='.urlencode($id).'&include_entities=true&result_type=recent&count=15
			$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/search/tweets'), array('q' => $data[1], 'include_entities' => true, 'include_rts' => false, 'result_type' => 'recent', 'count' => 15));
			$response = $tmhOAuth->response;

		endif;


		//Now we load the JSON into an associative array.  The first parameter is the response we got from the CURL session and the second boolean is for the decode to return an associative array.
		$tweets = json_decode($response['response'], true);

		if( $data[0] != 'timeline' ) $tweets = $tweets['statuses'];
		//print_r($tweets); exit;
		//I ran a count on the tweet array even though we set it to 5 just so we don't get an array index out of bounds error in the loop.
				$tweetCount = count($tweets);
				$counter = 0;

		//Iterates through all of the tweets and prints out all of the tweets received and the date they were created at.
			$tweet_arr = array();
				while($counter < $tweetCount){
					if( $data[0] != 'timeline' ) $prelink = 'https://twitter.com/'.$tweets[$counter]["screen_name"].'/status/';
					$tweet_item = array(
						  'desc' => $tweets[$counter]["text"],
						  'title' => $tweets[$counter]["text"],
						  'date' => $tweets[$counter]["created_at"],
						  'link' => $prelink.$tweets[$counter]["id_str"],
						  'photo' => $media = $tweets[$counter]["entities"]["media"][0]["media_url"],
				   );
				   array_push($tweet_arr, $tweet_item);
					$counter++;
				}
				//print_r($tweet_arr); exit;
			return $tweet_arr;
	}
	function fetch_instagram_json($data, $type ){
		$url = $data;
		//echo $url; exit;
		$html = file_get_contents($url);
		$arr = explode('window._sharedData = ',$html);
		$arr = explode(';</script>',$arr[1]);
		$obj = json_decode($arr[0] , true);
		//echo $type.'<pre>'.print_r($obj,1).'</pre>'; exit;
	
		$json_array = array();
		if( $type == 'Instagram'){
			$posts = $obj['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
		}
		if( $type == 'Instagram Hashtag'){
			$posts = $obj['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'];
		}

		//echo '<pre>'.print_r($posts,1).'</pre>'; exit;

		foreach( $posts as $p ){
			$postdata = $p['node'];
			if( isset($postdata['edge_media_to_caption']['edges'][0])){
            $title = $postdata['edge_media_to_caption']['edges'][0]['node']['text'];
            
          
			$desc = $postdata['edge_media_to_caption']['edges'][0]['node']['text']; //htmlentities($rss_update->description, ENT_QUOTES, "UTF-8");
            //$desc = htmlentities($desc, ENT_QUOTES, "UTF-8");
			//Converts UTF-8 into ISO-8859-1 to solve special symbols issues
			//$desc = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $desc);

			//Get status update time
			$pubdate = $postdata['taken_at_timestamp'];
			$propertime = gmdate('F jS Y, H:i', $pubdate);  //Customize this to your liking

			//Get link to update
			$linkback = 'https://www.instagram.com/p/'.$postdata['shortcode'];
		//	$linkback = (string)$linkback[0];

			$photo = $postdata['thumbnail_resources'][4]['src'];

			//Store values in array
			$json_item = array(
				 'desc' => $desc,
				 'title' => $title,
				 'date' => $propertime,
				 'link' => $linkback,
				 'photo' => $photo
			);
			array_push($json_array, $json_item);
		}
		}
		//echo '<pre>'.print_r($json_array,1).'</pre>'; exit;
	return $json_array;

	}
	function fetch_rss_feed($url, $maxnumber, $type){
		ini_set('user_agent', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS C 10.5; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3');
		$updates = simplexml_load_file($url);  //Load feed with simplexml
		//if( isset($_POST['social-update-now']) ) print_r($updates);
		if( $updates ):
		 $rss_array = array();  //Initialize empty array to store statuses
		 if( $type == 'Pinterest' || $type == 'Instagram' ):
		 foreach ($updates->channel->item as $rss_update) {
			  if ($maxnumber == 0) {
				   break;
			  } else {

				   $title = $rss_update->title;

				   $desc = $rss_update->description;
				  // $desc = htmlentities($desc, ENT_QUOTES, "UTF-8");

				   //Converts UTF-8 into ISO-8859-1 to solve special symbols issues
				   //$desc = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $desc);

				   //Get status update time
				   $pubdate = strtotime($rss_update->pubDate);
				   $propertime = gmdate('F jS Y, H:i', $pubdate);  //Customize this to your liking

				   //Get link to update
				   $linkback = $rss_update->link;
				   $linkback = (string)$linkback[0];

				   //Store values in array
				   $rss_item = array(
						  'desc' => $desc,
						  'title' => $title,
						  'date' => $propertime,
						  'link' => $linkback
				   );
				   array_push($rss_array, $rss_item);

				   $maxnumber--;
			  }
		 }
		 elseif($type == 'YouTube' ):
		 //	print_r($updates);
		 	foreach( $updates->entry as $entry ){
				$title = (string) $entry->title;
				$desc = $entry->children('media', true)->group->description;
				//Converts UTF-8 into ISO-8859-1 to solve special symbols issues
				   //$desc = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $desc);

				   //Get status update time
				   $pubdate = (string) $entry->published;
				   $propertime = gmdate('F jS Y, H:i', strtotime($pubdate));  //Customize this to your liking

				   //Get link to update
				   $linkback = $entry->link->attributes(); //->children()->href;
				   $linkback = (string)$linkback[1];

				   //Store values in array
				   $rss_item = array(
						  'desc' => $desc,
						  'title' => $title,
						  'date' => $propertime,
						  'link' => $linkback
				   );
				  // print_r($rss_item); exit;
				   array_push($rss_array, $rss_item);

				   $maxnumber--;
			}
		 else:
		 foreach($updates->entry as $rss_update){
			//if( isset($_POST['social-update-now']) ) print_r($rss_update);
			if( $maxnumber == 0 ){ break;
			}else{
				   $title = $rss_update->title;
				   $desc = $rss_update->content;
				   //Converts UTF-8 into ISO-8859-1 to solve special symbols issues
				   $desc = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $desc);

				    //Get status update time
				   $pubdate = strtotime($rss_update->published);
				   $propertime = gmdate('F jS Y, H:i', $pubdate);  //Customize this to your liking

				   //Get link to update
				   $linkback = $rss_update->link[0]->attributes();
				   $linkback = (string)$linkback[href];

				   //Store values in array
				   $rss_item = array(
						  'desc' => $desc,
						  'title' => $title,
						  'date' => $propertime,
						  'link' => $linkback
				   );
				   array_push($rss_array, $rss_item);

				   $maxnumber--;
			}
		 }
		 endif;
		 endif;

		 //Return array
		 return $rss_array;
	}

	function Quicklog($content, $add=false){
		$file = ABSPATH.'cronerr.log';
		$d = date('[F d, Y - h:ia]').' - ';
		$fh = fopen($file, 'a');
		fwrite($fh, $d.serialize($content).$add."\n");
		fclose($fh);

	}


	function importFeed($type, $url, $tag=false){
		global $wpdb;
		$rss = false;
		if( $type == 'Twitter' || $type == 'Twitter Search' ){
			$rss = $this->fetch_twitter_api($url, $type);
		}elseif( $type == 'Facebook' ){
			if( $url )	$rss = $this->fetch_facebook_json($url, $type);
		}elseif( $type == 'Instagram' ){
            $rss = $this->fetch_instagram_json($url, $type);
           // echo '<pre>'.$rss.'</pre>'; exit();
		}elseif( $type == 'Instagram Hashtag' ){
				$rss = $this->fetch_instagram_json($url.'/', $type);
		}else{
			$rss = $this->fetch_rss_feed($url, 10, $type);
		}
		$max = 10;
		$i = 1;
    
    
    if( $rss ){
   //echo '<h2>'.$type.'</h2><pre>'.print_r($rss,1).'</pre>'; exit;
		//if( isset($_POST['social-update-now']) ) echo "\n\n".'<br><br><h3>'.$type.':'.count($rss).' ('.$url.')</h3>';
		foreach( $rss as $t ):
			if( $i > $max ) break;
			$i++;
			if( $this->mode == 'wordpress') $term = get_term_by( 'name', $type, 'social' );
			//$this->Quicklog($term, ' | '.$type);
			$gmdate = gmdate('Y-m-d H:i:s', strtotime($t['date']));
			$ta = get_option("social_date_adjust");
			if( $ta ) $ta = " ".$ta;
			$date = date('Y-m-d H:i:s', strtotime($t['date'].$ta));
			$nicedate = date('g:ia F jS, Y', strtotime($t['date'].$ta));
			$photo = false;
			$ph = explode('src="', $t['desc']);
			if( is_array($ph) && count($ph) > 1 ){
				$ph = explode('"', $ph[1]);
				$photo = $ph[0];
			}
			if( !$photo ){
				$ph = explode("src=", $t['desc']);
				if( is_array($ph) && count($ph) > 1 ){
					$ph = explode("&gt;", $ph[1]);
					$ph = explode(' height=', $ph[0]);
					$photo = str_replace(array("'",'"', '&quot;'),'',$ph[0]);
				}
        
			}
      
//echo $photo.' = <img src="'.$photo.'"> <br>';

			if( $t['photo'] != '' ) {
				$photo = $t['photo'];
				if( $this->mode == 'standalone'){
					$pnm = explode('?',basename($photo));
					copy($photo, 'socialimg/'.$pnm[0]);
					$photo = '/socialimg/'.$pnm[0];
				}
			}

			if( $type == 'YouTube'){
				/*$desc = array();
				$dpattern = '#\<span\>(.+?)\<\/span\>#s';
				preg_match($dpattern, $t['desc'], $desc);
				$as = array();
				$apattern = '#\<a[^>]*href="(.+?)"[^>]*\>(.+?)\<\/a\>#s';
				preg_match_all($apattern, $t['desc'], $as);
				$t['link'] = $as[1][0]; ///youtube link
				$img = $as[0][0];*/
				$vid = explode('v=',$t['link']);
				$img = 'http://img.youtube.com/vi/'.$vid[1].'/hqdefault.jpg';
				$photo = $img;
				$t['desc'] = $t['title'];
			}
			$t['desc'] = str_replace('<span style="', '<span style="display:none;', $t['desc']);
			$tg = false;
    if( $tag ) $tg = '['.$tag.']';
			
					//$this->Quicklog($post);
			//if( $type == 'YouTube'){ print_r($post); exit;}
			//if( isset($_POST['social-update-now']) ) print_r($post);
			//exit();
	  $dupe = false;
	  if( $this->mode == 'wordpress'){
        $post = array(
            'post_title' => $nicedate.' ['.$type.']'.$tg,
            'post_content' => $t['desc'],
            'post_excerpt' => (string)$t['title'],
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' => 'social_post',
            'post_date' => $date,
            'post_date_gmt' => $gmdate,
            //'tax_input' => array( 'social' => $term->term_id ),
            //'link' => $t['link'],
            //'photo' => $photo
        );
			$dupe = $wpdb->get_var("SELECT post_title FROM $wpdb->posts WHERE post_type = 'social_post' AND post_title = '$nicedate [$type]$tg'");
	  }elseif( $this->mode == 'standalone'){
        $post = array(
            'title' => $nicedate.' ['.$type.']'.$tg,
            'content' => urlencode( sanitize_text_field($t['desc'])),
            'excerpt' => urlencode( sanitize_text_field((string)$t['title'])),
            'date' => $date,
            'date_gmt' => $gmdate,
            'type' => $type,
            'link' => $t['link'],
            'photo' => $photo
        );
		  $dupe = $this->check_dupe($nicedate.' ['.$type.']'.$tg);
	  }
			$last_word_start = strrpos ( $t['desc'], " ") + 2;
			$last_word = substr($t['desc'], $last_word_start);
			//$toosimilar = $wpdb->get_var("SELECT post_title FROM $wpdb->posts WHERE post_type = 'social_post' AND post_content LIKE \"".esc_textarea($this->truncate(strip_tags($t['desc']), 60))."%\"");

			//echo "SELECT post_title FROM $wpdb->posts WHERE post_type = 'social_post' AND post_content LIKE '".wp_trim_words($t['desc'], 3, false)."%' AND post_content LIKE '%".$last_word."'";
     // echo $post['post_title']. '||DUPE='.$dupe.'  ||2SIMILAR='.$toosimilar.'<br><br><br>';
    
			if( $dupe ){
				//if( isset($_POST['social-update-now']) ){ 
        //echo '<br> [Duplicate] <br>'; echo "SELECT post_title FROM $wpdb->posts WHERE post_type = 'social_post' AND post_content LIKE \"".$this->truncate($t['desc'], 60)."%\"<br><br>";
        //}
				continue;
			}else{
				 //echo '<pre>'.print_r($post,1).'</pre>';
				if( $this->mode == 'wordpress'){
						$post_ID = wp_insert_post($post, true);
						if( is_wp_error( $post_ID ) ) {
							error_log($post_ID->get_error_message());
						}
						//echo $post_ID.'<br>';
						if( $post_ID):
							$terms = array($type);
							if( $tag ){
								$terms = array($type,$tag);
							}
							//print_r($photo);
							if( $photo ) $photo = $this->sideload($photo, $post_ID);

							wp_set_object_terms($post_ID, $terms, 'social');
							update_post_meta($post_ID, 'social_link', $t['link']);
							if( $photo ){
								update_post_meta($post_ID, '_thumbnail_id', $photo);
								//update_post_meta($post_ID, 'social_photo', $photo);
							}

						endif;
					}elseif( $this->mode == 'standalone'){
						$post_ID = $this->insert_post($post);
					}
				}
			//if( isset($_POST['social-update-now']) ) echo '<br><br>';
		endforeach;
		}
		//if( $type == 'Facebook' ) exit;
	}
	function truncate($string,$length=100) {
      $string = trim($string);

      if(strlen($string) > $length) {
        $string = wordwrap($string, $length);
        $string = explode("\n", $string, 2);
        $string = $string[0];
      }

      return $string;
	}
	
	function check_dupe($title){
		$stmt = $this->db->prepare('SELECT title FROM socialfeed WHERE title = :title');
	 	$stmt->bindParam(':title', $title);	
		$stmt->execute();

	 	$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if( !$result ) return false;
		return $result['title'];
	}
	function insert_post($post){
        //echo '<pre>'.print_r($post).'</pre>';
		$stmt = $this->db->prepare('INSERT INTO socialfeed (id,title,content,excerpt,date,date_gmt,photo,link,type) VALUES (null, :title, :content, :excerpt, :date, :date_gmt, :photo, :link, :type)');
	 		$stmt->bindParam(':title', $post['title']);
	 		$stmt->bindParam(':content', $post['content']);
			$stmt->bindParam(':excerpt', $post['excerpt']);
			$stmt->bindParam(':date', $post['date']);
	 		$stmt->bindParam(':date_gmt', $post['date_gmt']);
	 		$stmt->bindParam(':photo', $post['photo']);
	 		$stmt->bindParam(':link', $post['link']);
			$stmt->bindParam(':type', $post['type']);
	 		$stmt->execute();	
			
			if( $stmt->errorCode() != 0 ){ $errors = $stmt->errorInfo(); $this->Quicklog($errors); }
			//echo '<pre>'.print_r($errors).'</pre>'; exit;
			return $this->db->lastInsertId();
	}

	function sideload($url, $post_id){
		$tmp = download_url( $url );
		if( is_wp_error( $tmp ) ) return false;
		$file_array = array();
		preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches);
		$file_array['name'] = basename($matches[0]);
		$file_array['tmp_name'] = $tmp;
		//$desc = '{imported with SocialFeed}';
		$desc = '';
		if ( is_wp_error( $tmp ) ) {
			@unlink($file_array['tmp_name']);
			$file_array['tmp_name'] = '';
		}
		// do the validation and storage stuff
		$id = media_handle_sideload( $file_array, $post_id, $desc );

		// If error storing permanently, unlink
		if ( is_wp_error($id) ) {
			@unlink($file_array['tmp_name']);
			return false;
		}

		return $id;
	}

	function schedule(){
		

		if( get_option('instagram_id') ){

			$ids = get_option('instagram_id');
            
            if( !is_array($ids)){ $ids = array($ids);}
             $tags = get_option('instagram_tag');
            //echo 'Facebook:<pre>'.print_r($tags,1).'</pre>'; exit;
            // echo $this->get_feed($id, 'facebook');
			foreach( $ids as $k => $id){
				 if( $id ) $this->importFeed('Instagram', $this->get_feed($id, 'instagram'), $tags[$k]);
			}
            /*//$ids = explode(',',str_replace(' ','', get_option('instagram_id')));
            //echo 'Instagram:<pre>'.print_r($ids,1).'</pre>';
           //echo $this->get_feed($id[0], 'instagram').'<br>';
			foreach( $ids as $id){
                 $this->importFeed('Instagram', $this->get_feed($id, 'instagram'));
                 //echo $this->get_feed($id, 'instagram').'<br>';
            }*/
		}

		if( get_option('instagram_hashtag') ){
			$ids = explode(',',str_replace(' ','', get_option('instagram_hashtag')));
			foreach( $ids as $id)
				 $this->importFeed('Instagram Hashtag', $this->get_feed($id, 'instagram_hashtag'));
		}

		if( get_option('facebook_id') ){
            $ids = get_option('facebook_id');
            
            if( !is_array($ids)){ $ids = array($ids);}
             $tags = get_option('facebook_tag');
            //echo 'Facebook:<pre>'.print_r($tags,1).'</pre>'; exit;
            // echo $this->get_feed($id, 'facebook');
			foreach( $ids as $k => $id){
				 if( $id ) $this->importFeed('Facebook', $this->get_feed($id, 'facebook'), $tags[$k]);
			}
		}

		if( get_option('twitter_id') ){
			$ids = explode(',',str_replace(' ','', get_option('twitter_id')));
			foreach( $ids as $id)
				$this->importFeed('Twitter', $this->get_feed($id, 'twitter'));
		}

		if( get_option('twitter_search') ){
			$ids = explode(',',str_replace(' ','', get_option('twitter_search')));
			foreach( $ids as $id)
				$this->importFeed('Twitter Search', $this->get_feed($id, 'twittersearch'));
		}

		if( get_option('youtube_id') ){
			$ids = explode(',',str_replace(' ','', get_option('youtube_id')));
			foreach( $ids as $id)
				$this->importFeed('YouTube', $this->get_feed($id, 'youtube'));
		}

		if( get_option('pinterest_id') ){

				$this->importFeed('Pinterest', $this->get_feed(get_option('pinterest_id'), 'pinterest', get_option("pinterest_board")));
        }
        if( $this->mode == 'standalone'){
            echo 'SOCIAL POSTS IMPORTED!';
            exit();
        }
        
		//if( isset($_POST['social-update-now']) ) die('<br><br><a href="'.$_SERVER['REQUEST_URI'].'">Continue...</a>');
	}

	function TimeAgo($time){
		//date_default_timezone_set('MST');
		//$localtime = strtotime(date('Y-m-d H:i:s',$time));
		$time = time() - $time;

		$tokens = array(
			31536000 	=> 'year',
			2592000 	=> 'month',
			604800 		=> 'week',
			86400 		=> 'day',
			3600  		=> 'hour',
			60 			=> 'minute',
			1 			=> 'second'
		);
		//echo "SECONDS=".$time;
		foreach( $tokens as $unit => $text ):
			//echo " TA=".floor($time / $unit );
			if( $time < $unit) continue;
			$numberOfUnits = floor($time / $unit );

			return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' ago';
		endforeach;
	}

	function GetLinkedDomain($url){
		$out = explode('http://', $url);
		$out = explode( '/', $out[1]);
		$out = $out[0];
		return $out;
	}

	function FeedLink($s){
		//return preg_replace('/https?:\/\/[\w\-\.!~?&+\*\'"(),\/]+/','<a href="$0">$0</a>',$s);
		$regex = '/http([s]?):\/\/([^\ \)(\s)$]*)/';
		$link_pattern = ' <a href="http$1://$2" rel="nofollow" target="_blank" title="$2">$2</a> ';
		$s = preg_replace($regex,$link_pattern,$s);
		$regex = '/@([a-zA-Z0-9_]*)/';
		$link_pattern = ' <a href="http://twitter.com/$1" title="View @$1\'s profile on Twitter" target="_blank" rel="nofollow">@$1</a> ';
		$s = preg_replace($regex,$link_pattern,$s);
		$regex = '/(^|\s)#(\w*[a-zA-Z_]+\w*)/';
		$link_pattern = ' <a href="http://search.twitter.com/search?q=%23$2" target="_blank" title="search for #$2 on Twitter" rel="nofollow">#$2</a> ';
		$s = preg_replace($regex,$link_pattern,$s);
		return $s;
	}

	function Shortcode($atts){
		extract( shortcode_atts( array(
			'limit' => 3,
			'single' => false,
			'template' => false,
			'wraptag' => true
		), $atts ) );
		return $this->SocialFeed($limit, $single, $template, $wraptag);
	}

	function SocialFeed($limit=3, $single=false, $template=false, $wraptag=true){
		global $post;

		
		$args = array(
			'post_type' => 'social_post',
			'orderby' => 'date',
			'order'	=> 'DESC',
			'posts_per_page' => $limit
		);
		$i=1;
		//echo $single;
		if( $single ){
      $single = explode(',',$single);
			$args['tax_query'] = array( array('taxonomy' => 'social', 'field' => 'slug', 'terms' => $single ) );
		}
		$dateformat = get_option("social_date_format");
		$dateopt = get_option("social_date_option");
		if( $dateopt == 'timeago' ) $timeago = true;
		//echo '<pre>'.print_r($args,1).'</pre>';

		if( $this->mode == 'wordpress'){
			$sq = new WP_Query($args);
			$out = '';
			//echo '<pre>'.print_r($sq,1).'</pre>';

			if ( $sq->have_posts() ) while ( $sq->have_posts() ) : $sq->the_post();
				//echo 'PD='.strtotime($post->post_date);
				if( $timeago ) $postdate = $this->TimeAgo(strtotime($post->post_date));
				else $postdate = date($dateformat, strtotime($post->post_date));

				$title = $post->post_excerpt;
				$content = str_replace('<br/>', '', urldecode($post->post_content));
				//$content = preg_replace("/<a[^>]*>(.*)<\/a>/iU", "", $content);
				$photo = false;
				$link = false;
				$terms = false;
				$thumb = false;
				$photo = get_post_meta($sq->post->ID, '_thumbnail_id', true);
				//if( $photo ) $photo = str_replace( "&", "&amp;", $photo);
				$link = get_post_meta($sq->post->ID, 'social_link', true);
				if( $link ) $link = str_replace( "&", "&amp;", $link);
		//convert all web.stagram links to instagram
		$link = str_replace('web.stagram.com', 'instagram.com', $link);
				$terms = wp_get_post_terms($sq->post->ID, 'social');
				//print_r($terms);
				$type = false;
				$class= array();
				if( $terms ){
					foreach($terms as $t){ 
						if( !$t->parent ) $type = $t->name; 
						$class[] = $t->slug;
					}
				}

				/*if( $class == 'twitter' ){
					$content = explode(':', $content);
					unset($content[0]);
					$content = implode(':', $content);
				}*/

				if( !$content ) $content = $title;
				$excerpt_length = get_option('social_template_excerpt');

				$lazy = get_option('social_lazyload');

				//$excerpt = preg_replace("/<a[^>]*>(.*)<\/a>/iU", "", $excerpt);
				$excerpt = preg_replace('/<img[^>]+\>/i', '',$content);

				$excerpt_notags = strip_tags($excerpt);


				$size = get_option('social_photo_size');
				//$imgs = array();
				//$img = preg_match_all('!https://[^?#]+\.(?:jpg|png|gif)!Ui', $excerpt, $imgs);

				$content = preg_replace('!https://[^?#]+\.(?:jpg|png|gif)!Ui', '',$content);
			//	if( !$photo && $imgs[0][0]) $photo = $imgs[0][0];
				$content = str_replace(array('<br>', '<br />'), '', $content);
				$content = str_replace('>', '> ', $content);
				$excerpt = wp_trim_words($content, $excerpt_length);
				$excerpt_notags = strip_tags($excerpt);
				$hasphoto = false;
				$aclass = false;
					$atts = false;
				
				if( $photo ){
					//$thumb = '<a class="img" href="'.$link.'" target="_blank" rel="nofollow" title="View this post on '.ucfirst($type).'"><img src="'.$photo.'" alt="" class="'.$class.'img" /></a>';
					$imgth = wp_get_attachment_image_src($photo, $size);
					$img = $imgth[0];
					if( $type == 'YouTube' ){
						$aclass = ' iLightBox youtubeplay';
						$atts = " data-type='iframe' data-options='icon: \"video\", width: 638, height: 360'";
						$tlink = $link;
						$link = str_replace('http:', '', $link);
						$link = str_replace('watch?v=', 'embed/', $link).'?autohide=1&border=0&egm=0&showinfo=0&showsearch=0';
					}
					if( !$lazy )
						$thumb = '<a class="img'.$aclass.'" href="'.$link.'" target="_blank"'.$atts.' rel="nofollow" title="View this post on '.ucfirst($type).'"><img src="'.$imgth[0].'" alt="" class="'.implode(' ',$class).' img" /></a>';
					else
						$thumb = '<a class="img'.$aclass.'" href="'.$link.'" target="_blank"'.$atts.' rel="nofollow" title="View this post on '.ucfirst($type).'"><img data-src="'.$imgth[0].'" alt="" class="'.implode(' ',$class).' img lazy" /></a>';
					$hasphoto = 'hasphoto';
					if( $type == 'YouTube' ){
						$link = $tlink;
					}
				}else{
					$img = false;
					$thumb = false;
				}
				$icon = '<img src="'.get_bloginfo('template_directory').'/img/'.$class[0].'.png" alt="" />';

				$photoicon = $thumb;
				if( !$photo ) $photoicon = '<a class="img'.$aclass.'" href="'.$link.'"'.$atts.' target="_blank" rel="nofollow" title="View this post on '.ucfirst($type).'">'.$icon.'</a>';

				//$content = $this->FeedLink($content);
				$content = str_replace('<br>', '', str_replace('', "&#39;", $content));
				//$excerpt = $this->FeedLink($excerpt);
				$excerpt = str_replace('<br>', '', str_replace('', "&#39;", $excerpt));
		
		// remove FetchRSS footer text instagram rss feed helper adds
		$excerpt = str_replace('(RSS generated with FetchRss )', '', $excerpt);
		$content = str_replace('(RSS generated with FetchRss )', '', $content);
		
		//remove FetchRSS image from instagram start
		$excerpt = preg_replace("/&lt;a href=&quot;&quot; height=&quot;[0-9]*&quot; width=&quot;[0-9]*&quot;&gt;&lt;\/a&gt;&lt;br\/&gt;(.*)/", "$1", $excerpt);
		$content = preg_replace("/&lt;a href=&quot;&quot; height=&quot;[0-9]*&quot; width=&quot;[0-9]*&quot;&gt;&lt;\/a&gt;&lt;br\/&gt;(.*)/", "$1", $content);
	
	
		
		
				//OUTPUT TEMPLATE
				$tag = get_option('social_template_wrap');
				$wclass = get_option('social_template_wrap_class');

				$content = $this->AutoLink($content);
				$excerpt = $this->AutoLink($excerpt);
				$actions = false;
				//Social Actions
				if( $type == 'Facebook' ):
					$actions = '<a href="'.$link.'" target="_blank">LIKE/SHARE/COMMENT</a>';
				elseif( $type == 'Twitter' ):
					$actions = '<a href="'.$link.'" target="_blank">REPLY/RETWEET/FAVOURITE</a>';
				elseif( $type == 'Instagram' ):
					$actions = '<a href="'.$link.'" target="_blank">LIKE/COMMENT</a>';
				endif;
				$plid = get_option('social_plimg');
				$plimg = wp_get_attachment_image_src($plid, get_option('social_photo_size'));
				if( !$thumb && $plid ) $thumb = '<a class="img'.$aclass.' placeholder" href="'.$link.'" target="_blank"'.$atts.' rel="nofollow" title="View this post on '.ucfirst($type).'"><img src="'.$plimg[0].'" alt=""></a>';
				if( !$img && $plid ) $img = $plimg[0];
				if( !$template ) $template = get_option('social_template');
				$codes = array( '[sourcelink]', '[service]', '[icon]', '[postdate]', '[photo]', '[photo|icon]', '[excerpt]', '[excerpt_notags]', '[content]', '[img]', '[actions]' );
				$replacements = array( $link, $type, $icon, $postdate, $thumb, $photoicon, $excerpt, $excerpt_notags, $content, $img, $actions);
				$post = str_replace($codes, $replacements, $template);
				$class = implode(' ',$class);
				if( $wraptag ) $out .= "<$tag class='$wclass $class $hasphoto p{$i}'>".$post."</$tag>";
				else $out .= $post;

				$i++;
			endwhile;
			wp_reset_query();

//STANDALONE
		}elseif( $this->mode == 'standalone'){


			$stmt = $this->db->prepare('SELECT * FROM socialfeed ORDER BY date DESC LIMIT '.$limit);
			$stmt->execute();

            $result = $stmt->fetchAll();
            
            if( $_GET['socialtest'] == 1){
                echo '<pre>'.print_r($result,1).'</pre>';
                exit;
            }
			
			$i=1;
			foreach( $result as $post ):
				if( $timeago ) $postdate = $this->TimeAgo(strtotime($post['date']));
				else $postdate = date($dateformat, strtotime($post['date']));
				$title = $post['excerpt'];
				$content = str_replace('<br/>', '', $post['content']);
				$photo = false;
				$link = false;
				$terms = false;
				$thumb = false;
				$photo = $post['photo'];
				$link = $post['link'];
				if( $link ) $link = str_replace( "&", "&amp;", $link);
				
				if( !$content ) $content = $title;
                $excerpt_length = get_option('social_template_excerpt');
				$type = $post['type'];
				//$excerpt = preg_replace("/<a[^>]*>(.*)<\/a>/iU", "", $excerpt);
				$excerpt = preg_replace('/<img[^>]+\>/i', '',$content);
				$imgs = array();
				$img = preg_match_all('!https://[^?#]+\.(?:jpg|png|gif)!Ui', $excerpt, $imgs);
				$content = preg_replace('!https://[^?#]+\.(?:jpg|png|gif)!Ui', '',$content);
				if( !$photo && $imgs[0][0]) $photo = $imgs[0][0];
				$content = str_replace(array('<br>', '<br />'), '', $content);
				$content = str_replace('>', '> ', $content);
				$excerpt = $this->trim_words($content, $excerpt_length);
				$hasphoto = false;
				if( $photo ){ $thumb = '<a class="img" href="'.$link.'" target="_blank" rel="nofollow" title="View this post on '.ucfirst($type).'"><img src="'.$photo.'" alt="" class="'.$class.'img" /></a>'; 
					$hasphoto = 'hasphoto';
				}else{
						
				}
				//$icon = '<img src="'.get_bloginfo('template_directory').'/img/'.$class.'.png" alt="" />';
				
				$photoicon = $thumb;
				//if( !$photo ) $photoicon = '<a class="img" href="'.$link.'" target="_blank" rel="nofollow" title="View this post on '.ucfirst($type).'">'.$icon.'</a>';
				
				//$content = $this->StreamLink($content);
				$content = str_replace('<br>', '', str_replace('', "&#39;", $content));
				//$excerpt = $this->StreamLink($excerpt);
				$excerpt = str_replace('<br>', '', str_replace('', "&#39;", $excerpt));
				//OUTPUT TEMPLATE
				$tag = "div";
		
                $template = get_option('social_template');
				
				if( !$template ) $template = "<p><span class='excerpt'>[excerpt]</span></p><p><span class='thumb'>[photo]</span><br><a class='date' href='[sourcelink]' target='_blank' rel='nofollow' title='View this post on [service]'>View on [service]</a></p>";

				$codes = array( '[sourcelink]', '[service]', '[icon]', '[postdate]', '[photo]', '[photo|icon]', '[excerpt]', '[excerpt_notags]', '[content]', '[img]' );
				$replacements = array( $link, $type, $icon, $postdate, $thumb, $photoicon, $excerpt, $excerpt_notags, $content, $photo);
				$post = str_replace($codes, $replacements, $template);
				if( $wraptag ) $out .= "<$tag class='social $class $hasphoto p{$i}'>".$post."</$tag>";
				else $out .= $post;
	
				$i++;
			endforeach;
		}

		return $out;

    }
    
    function trim_words( $text, $num_words = 10, $more = null ) {
		if ( null === $more )
			$more = '&hellip;';
		$original_text = $text;
		$text = $this->strip_all_tags( $text );
		/* translators: If your word count is based on single characters (East Asian characters),
		   enter 'characters'. Otherwise, enter 'words'. Do not translate into your own language. */
		if ( 'characters' == 'words' ) {
			$text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );
			preg_match_all( '/./u', $text, $words_array );
			$words_array = array_slice( $words_array[0], 0, $num_words + 1 );
			$sep = '';
		} else {
			$words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );
			$sep = ' ';
		}
		if ( count( $words_array ) > $num_words ) {
			array_pop( $words_array );
			$text = implode( $sep, $words_array );
			$text = $text . $more;
		} else {
			$text = implode( $sep, $words_array );
		}
		return $text;
	}
	function strip_all_tags($string, $remove_breaks = false) {
		$string = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $string );
		$string = strip_tags($string);
	
		if ( $remove_breaks )
			$string = preg_replace('/[\r\n\t ]+/', ' ', $string);
	
		return trim( $string );
	}

	function AutoLink($text){
		// force http: on www.
		$text = preg_replace( "#www\.#", "http://www.", $text );
		// eliminate duplicates after force
		$text = preg_replace( "#http://http://www\.#", "http://www.", $text );
		$text = preg_replace( "#https://http://www\.#", "https://www.", $text );

		// The Regular Expression filter
		$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
		// Check if there is a url in the text
		if(preg_match($reg_exUrl, $text, $url)) {
			   // make the urls hyper links
			   $text = preg_replace($reg_exUrl, '<a href="'.$url[0].'" rel="nofollow" target="_blank">'.$url[0].'</a>', $text);
		}    // if no urls in the text just return the text
			   return ($text);
		}

}
$social = new SocialParser();
function mySocialFeed($limit=3, $single=false, $template=false, $wraptag=true){
	global $social;
	return $social->SocialFeed($limit, $single, $template, $wraptag);
}
function mySocialStream($limit=3, $single=false, $template=false, $wraptag=true){
	global $social;
	return $social->SocialFeed($limit, $single, $template, $wraptag);
}